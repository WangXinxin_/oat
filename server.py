"""
本文件在开发时调试用
"""
import grpc
import time
from concurrent import futures

from client import local_host_ip
from handlers.oat_request import AgentGreeter
from common.rpc import agent_pb2_grpc


if __name__ == '__main__':
    # agent端
    # TODO: 待优化 启动多进程，开启多个线程
    localhost = local_host_ip()
    print(f"localhostlocalhost------>{localhost}")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=50))
    agent_pb2_grpc.add_AgentGreeterServicer_to_server(AgentGreeter(), server)
    server.add_insecure_port(f"{localhost}:{50052}")
    server.start()
    while 1:
        time.sleep(1)



