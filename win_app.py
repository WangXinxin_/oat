# -*- coding: utf-8 -*-
import os
import sys
import time
import platform
import socket
import threading
import win32serviceutil
import win32service
import win32event
import winerror
import servicemanager

from flask import Flask, jsonify, request

from config import APP_PORT, HOST_IP
from utils.agent_log import LOGGER
from result import Result
from handlers.processing import generate_identity, get_host_info, get_node_info, heartbeat
from handlers.win_handler import gen_win_whitelist, save_host


app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/get_host_status', methods=['GET'])
def get_local_status():
    """
    获取本机最新pcr值以及ima
    :return:
    """
    # result = Result(1, None, None)
    result = {'code': 1, 'msg': ''}
    try:
        # ima 行数
        ima_line = request.form.get('ima_seek', None)
        ima_line = int(ima_line)

        node_status = get_node_info(ima_line)

        if node_status.startswith("error"):
            result['code'] = 1
            result['msg'] = node_status
            result = str(result)
        else:
            result['code'] = 0
            result['msg'] = "success"
            result = str(result) + "\n" + node_status
    except Exception as e:
        # result.msg = str(e)
        result['code'] = 1
        result['msg'] = str(e)
        LOGGER.debug(e)

    # 将result转换成字符串
    if isinstance(result, dict):
        result = str(result)
    # return jsonify(result.to_dict())
    return result


@app.route('/get_win_list', methods=['GET'])
def get_win_whitelist():
    result = Result(1, "system is not win", None)
    # 判断操作系统
    os_name = platform.system().upper()
    if os_name == "WINDOWS":
        # 生成win白名单
        wl = gen_win_whitelist()
        result.code = 0
        result.msg = 'success'
        result.result = wl

    return jsonify(result.to_dict())


@app.route('/get_host_info', methods=['GET'])
def get_local_info():
    """
    获取本地信息 主机名 主机identity
    :return:
    """
    result = Result(1, "get host info failed.", None)
    try:
        local_info = get_host_info()

        if local_info['host_name'] and local_info['identity']:
            result.code = 0
            result.msg = "success"
            result.result = local_info
    except Exception as e:
        LOGGER.error(e)
        result.msg = str(e)

    return jsonify(result.to_dict())


class PythonService(win32serviceutil.ServiceFramework):
    _svc_name_ = "OctaAgent"  # 服务名
    _svc_display_name_ = "Tagent"  # 服务在windows系统中显示的名称
    _svc_description_ = "Trust Compute Agent"  # 服务的描述

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        self.run = True
        self.count = 0

    def SvcDoRun(self):
        while self.run:
            if self.count == 0:
                # 生成私钥对
                generate_identity()
                # 获取主机名
                host_name = socket.gethostname()
                # 将该机器添加到数据库中
                save_host(HOST_IP, host_name)
                # 启动心跳服务
                threading.Thread(target=heartbeat, args=(18,)).start()
                # 启动app
                app.run(host='0.0.0.0', port=APP_PORT, debug=True, use_reloader=False, threaded=True)

                self.count += 1
            else:
                time.sleep(2)

    def SvcStop(self):
        # 服务已经停止
        LOGGER.info("OctaAgent Server is stoping...")
        os.system('taskkill /f /IM tagent.exe /t')
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)
        self.run = False


if __name__ == "__main__":
    if len(sys.argv) == 1:
        try:
            evtsrc_dll = os.path.abspath(servicemanager.__file__)
            servicemanager.PrepareToHostSingle(PythonService)
            servicemanager.Initialize('PythonService', evtsrc_dll)
            servicemanager.StartServiceCtrlDispatcher()
        except win32service.error as details:
            details = eval(str(details))
            LOGGER.error(details)
            if details[0] == winerror.ERROR_FAILED_SERVICE_CONTROLLER_CONNECT:
                win32serviceutil.usage()
    else:
        win32serviceutil.HandleCommandLine(PythonService)
