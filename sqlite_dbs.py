import sqlite3
from utils.agent_log import LOGGER


def conn_sqlite():
    try:
        conn = sqlite3.connect("agent_dbs.db")
        return conn
    except Exception as e:
        LOGGER.error(e)


conn = conn_sqlite()


def create_table_agent_file_record():
    drop_tables("agent_ima_record")
    with conn:
        conn.execute("""
        CREATE TABLE agent_ima_record (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        file_name VARCHAR UNIQUE,
        current_seek INTEGER,
        curr_line_num INTEGER,
        describe TEXT 
        );
        """)


def create_table_host_info():
    drop_tables("host_info")
    with conn:
        conn.execute("""
        CREATE TABLE host_info (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        host_ip VARCHAR UNIQUE,
        reboot_time INTEGER,
        describe TEXT
        );
        """)


def drop_tables(table_name):
    with conn:
        conn.execute(f"drop table if exists {table_name};")


def create_tables():
    create_table_host_info()
    create_table_agent_file_record()
