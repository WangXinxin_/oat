# -*- coding: utf-8 -*-
import os
import platform

from utils.constants import USE_MQ
from utils.net_util import get_host_ip, get_ip_by_nic
from configparser import ConfigParser
from apscheduler.schedulers.background import BackgroundScheduler


scheduler = BackgroundScheduler()
# 系统类型 暂支持linux和windows
sys_name = platform.system().upper()
base_path = os.path.split(os.path.realpath(__file__))
if sys_name == 'LINUX':
    # tagent文件夹路径
    config_path = "/8lab/conf/oat_agent"
    AGENT_PATH = os.sep + "8lab" + os.sep + "tagent"
    # linux identity 文件夹路径
    IDENTITY_PATH = AGENT_PATH + os.sep + "identity"
elif sys_name == 'WINDOWS':
    # 获取系统盘符
    sys_drive = os.getenv("SystemDrive")
    # 配置文件路径
    config_path = sys_drive + os.sep + "8lab" + os.sep + "conf" + os.sep + "oat_agent"
    # tagent文件夹路径
    AGENT_PATH = sys_drive + os.sep + "8lab" + os.sep + "tagent"
    # windows identity 文件夹路径
    IDENTITY_PATH = AGENT_PATH + os.sep + "identity"
else:
    raise Exception("Only supports linux and windows.")

# 判断文件夹是否存在 不存在则创建
if not os.path.exists(config_path):
    os.makedirs(config_path)

if not os.path.exists(AGENT_PATH):
    os.makedirs(AGENT_PATH)

if not os.path.exists(IDENTITY_PATH):
    os.makedirs(IDENTITY_PATH)

# ml文件路径
MEASUREMENTS_PATH = "/sys/kernel/security/ima/ascii_runtime_measurements"

# 1.创建配置解析器对象
config = ConfigParser()
# 2.读取配置文件名
conf_path = config_path + os.sep + "tagent.ini"

if sys_name == "WINDOWS":
    config.read(conf_path, encoding='utf-8-sig')       # windows文本文件有bom标识 所以encode用utf-8-sig
else:
    config.read(conf_path, encoding='utf-8')


# 如果配置网卡名 通过网卡名获取
network_card = config.get('agent', 'agent_network_card')
if network_card != "no_card":
    AGENT_REAL_IP = get_ip_by_nic(network_card)
else:
    # 如果没有配置网卡名 通过udp获取agent真实地址 用于启动
    AGENT_REAL_IP = get_host_ip()

# agent ip是对外地址
AGENT_IP = config.get('agent', 'agent_mapping_ip')
# 如果没有配置映射地址 那么agent ip就是实际地址
if AGENT_IP == "no":
    AGENT_IP = AGENT_REAL_IP

# agent服务端口号
AGENT_PORT = config.getint('agent', 'agent_port')
# 映射端口号
AGENT_MAPPING_PORT = config.getint('agent', 'agent_mapping_port')
if AGENT_MAPPING_PORT == 0:     # 如果为0 表示没有映射端口号
    AGENT_MAPPING_PORT = AGENT_PORT

TRANSFER_MODE = config.get('open_mq', 'open_mq')
if TRANSFER_MODE == USE_MQ:
    RABBITMQ_IP = config.get('rabbitmq', 'mq_ip')
    RABBITMQ_PORT = config.get('rabbitmq', 'mq_port')
    RABBITMQ_USER = config.get('rabbitmq', 'mq_user')
    RABBITMQ_PASS = config.get('rabbitmq', 'mq_pass')


# oat server
OAT_IP = config.get('oat', 'oat_ip')
OAT_PORT = config.getint('oat', 'oat_port')

# 白名单及告警条数
WHITELIST_NUM = config.getint("whitelist_num", "whitelist_num")

# 心跳时间间隔
HEARTBEAT = config.getint('heartbeat', 'hb_interval')

# 是否开启逃生机制
OPEN_LIMIT = config.get('limit', 'open_limit')
# CPU上限
UPPER_LIMIT = config.getint('limit', 'upper_limit')
# CPU下限
LOWER_LIMIT = config.getint('limit', 'lower_limit')

if sys_name == 'LINUX':
    # audit.log 路径 只有linux系统需要操作审计日志
    AUDIT_PATH = config.get('audit', 'audit_path')
    AUDIT_INTERVAL = config.getint('audit', 'audit_interval')

