# -*- coding: utf-8 -*-
import psutil

from utils.agent_log import LOGGER
from utils.calc_hash import calc_sha1


def gen_win_whitelist():
    """
    生成windows白名单
    """
    result = {'code': 1, 'data': None, 'msg': None}
    try:
        white_list = {}
        for proc in psutil.process_iter():
            p_info = proc.as_dict(attrs=['pid', 'exe'])
            exe_path = p_info['exe']
            if exe_path:
                exe_hash = calc_sha1(exe_path)
                white_list[exe_path] = {'hash': exe_hash, 'pid': p_info['pid']}
        result['code'] = 0
        result['msg'] = 'success'
        result['data'] = white_list
    except Exception as e:
        result['msg'] = str(e)
        LOGGER.error(e)

    return result


# def save_host(host, host_name):
#     """
#     保存主机到mysql中 用于前端可信证实页面显示该主机
#     :param host: 本机ip地址
#     :param host_name: 主机名
#     :return:
#     """
#     conn = None
#     cursor = None
#     host_tag = '被保护机器'
#     try:
#         # 初始化数据库
#         conn, cursor = sql_init(DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_PORT)
#
#         # 判断该主机是否存在
#         query_sql = "SELECT hostname, os FROM app_fuzhou_blackbox_host WHERE hostip = '" + host + "'"
#         cursor.execute(query_sql)
#         query_data = cursor.fetchall()
#
#         if len(query_data) == 0:
#             # 保存host到数据库中 用于可信证实前端显示和控制   os 0 linux  1 windows
#             insert_sql = "INSERT INTO app_fuzhou_blackbox_host (hostip, hostname, description, status," \
#                          " blackbox, trustedstate, tagent, os) VALUES " \
#                          "('" + host + "','" + host_name + "','" + host_tag + "',1, '" + BLACKBOX_HOST + "', 0, 1, 1)"
#
#             LOGGER.info("insert win host, ip:" + host + " name:" + host_name)
#             cursor.execute(insert_sql)
#             conn.commit()
#         elif len(query_data) == 1:
#             win_hostname = query_data[0][0]
#             os_type = query_data[0][1]
#             if win_hostname != host_name or int(os_type) != 1:
#                 update_sql = "UPDATE app_fuzhou_blackbox_host SET hostname = '" + host_name + "'," \
#                              + " os = 1 " + "WHERE hostip = '" + host + "'"
#
#                 LOGGER.info("update win host, ip:" + host + " name:" + host_name)
#                 cursor.execute(update_sql)
#                 conn.commit()
#         else:
#             LOGGER.error("host data error, duplicate host ip.")
#     except Exception as e:
#         LOGGER.error(e)
#     finally:
#         if conn and cursor:
#             sql_close(conn, cursor)
