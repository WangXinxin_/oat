# -*- coding: utf-8 -*-
import json
import os
import time
import hashlib
import socket
import base64
import pathlib
import platform
import traceback

import grpc
import psutil

from Crypto import Random
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5 as Signature_pkcs1_v1_5
from retrying import retry

from common.rpc import oat_pb2_grpc, oat_pb2
from config import IDENTITY_PATH, MEASUREMENTS_PATH, AGENT_PORT, OAT_IP, OPEN_LIMIT, UPPER_LIMIT, \
    LOWER_LIMIT, OAT_PORT, AGENT_IP, TRANSFER_MODE, scheduler, WHITELIST_NUM
from sqlite_dbs import conn_sqlite
from utils.agent_log import LOGGER
from utils.constants import success, not_restarted, restarted, IMA_EXCHANGE, ALARM_QUEUE, ALARM_ROUTING, USE_MQ, \
    WHITELIST_QUEUE, WHITELIST_ROUTING, Action, READ_IMA
from utils.sql_utils import get_current_seek, update_seek
from utils.whitelist_utils import latest_seek_rows
from utils.cpu_calc import CpuUsage

# 保存本地pcr计算情况 {old:{seek: pcr, seek:pcr} 每10000行保存一次
LATEST_LOCAL_PCR = {'new': {'seek': 0, 'pcr': "0" * 40},
                    'old': {}
                    }


def calc_local_pcr():
    """
    计算本地pcr
    :return:
    """

    pos = LATEST_LOCAL_PCR["new"]["seek"]
    local_pcr = LATEST_LOCAL_PCR["new"]["pcr"]
    while True:
        # 5秒更新一次pcr值
        time.sleep(5)

        # line_num 标识读取的行数
        line_num = 0

        try:
            with open(MEASUREMENTS_PATH, "rb") as fd:
                if pos != 0:
                    fd.seek(pos, 0)

                for line in fd:
                    # byte转string
                    line = line.decode("utf-8")
                    # 获取最新游标值
                    pos = fd.tell()
                    # 每读10000行 暂停0.1秒
                    line_num = line_num + 1

                    line_content = line.strip()
                    m_list = line_content.split(" ")
                    if len(m_list) >= 5:  # 标准度量文件 至少有5个元素
                        # 获取文件路径 判读是否时tmp文件 因为有的系统ima依然度量tmp导致大量无用tmp
                        file_path = m_list[4]
                        if file_path.startswith("/tmp"):
                            LOGGER.info("tmp file:" + file_path)
                        else:
                            # PCR[i] = SHA-1(PCR[i-1] || new-measurement)
                            hash_method = hashlib.sha1()
                            hash_method.update(bytes.fromhex(local_pcr))

                            new_measurement = m_list[1]
                            # 如果new_measurement值是0，则替换为全F
                            if new_measurement == "0" * 40:
                                new_measurement = "F" * 40

                            hash_method.update(bytes.fromhex(new_measurement))
                            # 重新计算pcr值
                            local_pcr = hash_method.hexdigest()
                            # 更新本地最新pcr值
                            LATEST_LOCAL_PCR["new"]["pcr"] = local_pcr

                        if line_num % 10000 == 0:
                            # 每一万行保存一次
                            LATEST_LOCAL_PCR["old"][pos] = local_pcr
                            time.sleep(0.1)
                    else:
                        LOGGER.error("measure list format error...")

                    # 更新本地seek
                    LATEST_LOCAL_PCR["new"]["seek"] = pos

                    if OPEN_LIMIT == 'ON':  # 判断cpu限制是否打开
                        cpu_usage = CpuUsage.get_cpu_usage()
                        if cpu_usage > 20:
                            time.sleep(30)
                        elif cpu_usage > 5:
                            time.sleep(15)
        except Exception as e:
            LOGGER.error(e)


def get_win_node_info(ima_seek=0):
    """
    获取节点最新ima
    :param ima_seek: 读取的ima起始点
    :return:
    """
    ima_list = []

    local_seek = LATEST_LOCAL_PCR["new"]["seek"]
    local_pcr = LATEST_LOCAL_PCR["new"]["pcr"]
    mid_seek = local_seek  # 返回中间seek

    # 因为ima seek是递增的 而且ima_seek是记录的上次读取位置
    # 所以ima_seek不可能比本地的seek大 如果比本地seek大说明出错 有可能本地重启过
    if ima_seek < local_seek:
        # 每次证实最多返回10000行 old中key是每1万行的seek 按顺序从小到大
        old_seeks = list(LATEST_LOCAL_PCR['old'].keys())
        if len(old_seeks) > 0:
            # 从小到大排序
            sorted_seeks = sorted(old_seeks)
            if ima_seek >= int(sorted_seeks[-1]):  # 如果seek大于最大的seek
                ima_list = get_ima_list(ima_seek, local_seek, ima_list)
            else:
                for seek in sorted_seeks:
                    if ima_seek < int(seek):
                        mid_seek = int(seek)
                        ima_list = get_ima_list(ima_seek, seek, ima_list)
                        break
        else:  # old中没有时说明没有10000行
            ima_list = get_ima_list(ima_seek, local_seek, ima_list)
    elif ima_seek == local_seek:
        ima_list = []
    else:
        error_msg = "error, agent seek:" + str(local_seek) + " smaller than request seek:" + str(ima_seek)
        error_msg = {"code": 1, 'msg': error_msg}
        return error_msg

    # 对pcr进行签名
    sig = encrypt_msg(local_pcr)
    # 返回结果

    node_info = {"pcr": local_pcr, "sig": sig, "new_seek": mid_seek, "local_seek": local_seek, "ml": ima_list}

    return node_info


def get_ima_info(start_seek=0):
    LOGGER.info(f"Start reading ima seek from:{start_seek}")
    """
    获取linux节点最新ima,10秒发送一次到MQ
    :param start_seek: 读取的ima起始点
    :param current_lines: 读取的ima seek值对应的行号
    :return:
    """
    try:
        ima_list = []
        with open(MEASUREMENTS_PATH, "rb") as ml:
            # 设置起始游标
            ml.seek(start_seek, 0)
            line_num = 0
            for line in ml:
                # byte转string
                line = line.decode("utf-8")
                # 获取当前最新的游标值
                current_seek = ml.tell()
                line_content = line.strip()
                m_list = line_content.split(" ")
                if len(m_list) >= 5:  # 标准度量文件 至少有5个元素
                    # 获取文件路径 判读是否时tmp文件 因为有的系统ima依然度量tmp导致大量无用tmp
                    file_path = m_list[4]
                    if file_path.startswith("/tmp"):
                        pass
                    else:
                        file_hash = m_list[3]
                        file_info = get_file_info(file_path)
                        if file_info:  # 只传已存在的文件，那些不存在的文件就不传了
                            file_info.update({"file_path": file_path, "file_hash": file_hash})
                            ima_list.append(file_info)

                line_num = line_num + 1
                if line_num % 500 == 0 and OPEN_LIMIT == 'ON':  # 每500行判断一次cpu使用率
                    cpu_usage = CpuUsage.get_cpu_usage()
                    if cpu_usage > UPPER_LIMIT:
                        LOGGER.warning("warning: the pc work more than " + str(UPPER_LIMIT) + "%...")
                        # cpu使用率超过上限直接返回
                        break
                    elif cpu_usage > LOWER_LIMIT:
                        LOGGER.warning("warning: the pc work more than " + str(LOWER_LIMIT) + "%...")
                        time.sleep(3)

                if line_num == WHITELIST_NUM:  # 只传递配置文件设置的行数
                    break

            local_pcr = LATEST_LOCAL_PCR['new']['pcr']
            # 对pcr进行签名
            sig = encrypt_msg(local_pcr)
        host_name = "-"
        if socket.gethostname():
            host_name = socket.gethostname()
        node_info = {
            "host_ip": AGENT_IP, "pcr": local_pcr, "sig": sig, "port": AGENT_PORT,
            "host_name": host_name,
            "curr_seek": current_seek, "line_num": line_num, "ima_list": ima_list}
        return node_info, current_seek, line_num
    except Exception as e:
        LOGGER.error(e)


def get_file_info(file_path):
    """
    :param file_path: 要扫描的文件路径
    :return:
    """
    try:
        is_exit = os.path.exists(file_path)
        result = {}
        if is_exit:
            cmd_ret = os.popen(f'stat {file_path}').readlines()
            if cmd_ret:
                info = [i.split() for i in cmd_ret]

                result.update({
                    "file_size": info[1][1],
                    "file_type": f"{info[1][-2]} {info[1][-1]}",
                    "access_auth": info[3][1].split("/")[1].replace(")", ""),
                    "uid": int(info[3][4].strip("/")),
                    "gid": int(info[3][8].strip("/")),
                    "last_visit_time": f"{info[4][1]} {info[4][2][:8]}",
                    "modify_file_time": f"{info[5][1]} {info[5][2][:8]}",
                    "file_version": info[-1][-1]
                })
        return result
    except Exception as e:
        LOGGER.error(e)


def get_ima_list(from_seek, end_seek, ima_list):
    """
    获取最新的ima_list
    :param from_seek: 起始seek
    :param end_seek: 结束end
    :param ima_list: 本次证实读取的ima文件
    :return: 返回最新的ima_list
    """
    try:
        with open(MEASUREMENTS_PATH, "rb") as ml:
            # 设置起始游标
            ml.seek(from_seek, 0)

            line_num = 0
            for line in ml:
                # 防止读写抢占太多资源 每一万行sleep一次
                line_num = line_num + 1
                if line_num % 10000 == 0:
                    time.sleep(0.01)

                # byte转string
                line = line.decode("utf-8")
                # 获取最新游标值
                ima_seek = ml.tell()

                # 如果ima_seek大于end_seek结束
                if ima_seek > end_seek:
                    break

                line_content = line.strip()
                m_list = line_content.split(" ")
                if len(m_list) >= 5:  # 标准度量文件 至少有5个元素
                    # 获取文件路径 判读是否时tmp文件 因为有的系统ima依然度量tmp导致大量无用tmp
                    file_path = m_list[4]
                    if file_path.startswith("/tmp"):
                        LOGGER.info("tmp file not measured.")
                    else:
                        ima_list.append(line)
    except Exception as e:
        LOGGER.error(e)

    return ima_list


# 一旦发生给oat_server传送异常时，一共尝试三次每次间隔3秒
@retry(stop_max_attempt_number=3, wait_fixed=3000, retry_on_exception=lambda x: isinstance(x, grpc.RpcError))
def send_ima(action, ima_info):
    if TRANSFER_MODE == USE_MQ:
        from utils.rmq_util import pub_msg
        if action == Action.UPDATE_WHITELIST:
            exchange = IMA_EXCHANGE
            queue = WHITELIST_QUEUE
            routing_key = WHITELIST_ROUTING
        else:
            exchange = IMA_EXCHANGE
            queue = ALARM_QUEUE
            routing_key = ALARM_ROUTING
        pub_msg(ima_info, exchange, queue, routing_key)
    else:
        with grpc.insecure_channel(f"{OAT_IP}:{OAT_PORT}") as channel:
            stub = oat_pb2_grpc.GreeterStub(channel)
            if action == Action.UPDATE_WHITELIST:
                resp = stub.Whitelist(oat_pb2.WhitelistRequest(whitelist=ima_info))
                LOGGER.info(resp.status)
            else:
                resp = stub.Alarmlist(oat_pb2.AlarmlistRequest(alarmlist=ima_info))
                LOGGER.info(resp.status)


def read_ima(action, start_seek, init_seek, curr_lines, end_seek, start_lines, timestamp, total_lines=0):
    curr_ima_seek = init_seek
    while start_seek <= end_seek - 1:
        ima_info, ima_seek, line_num = get_ima_info(start_seek)
        curr_lines += line_num
        ima_info["curr_lines"] = curr_lines  # seek值对应的值
        ima_info["timestamp"] = timestamp  # 更新白名单的时间戳
        LOGGER.info(
            f"{action} current_seek--->{ima_seek} line_num--->{line_num} \n"
            f"ima_list_length--->{len(ima_info['ima_list'])} timestamp--->{ima_info['timestamp']}")
        start_seek = ima_seek
        curr_ima_seek = ima_seek
        if action == Action.UPDATE_WHITELIST:
            ima_info["total_lines"] = total_lines  # 文件总行数
            ima_info["start_lines"] = start_lines  # 本次读取文件行数
            ima_info["total_seek"] = end_seek  # 本次读取文件行数
            ima_info["start_seek"] = init_seek  # 请求更新白名单时的start_seek
        send_ima(action, json.dumps(ima_info))
    # 缓存curr_seek值和current_line_num
    update_seek(MEASUREMENTS_PATH, curr_ima_seek, curr_lines)


def ima_to_server(action, start_seek, end_seek, start_lines, timestamp, total_lines=0):
    """
    截取ima文件内容 目前供开启防护和更新白名单使用
    :param action: 执行动作是更新白名单还是开启防护
    :param start_seek: 读取ima文件的开始位置
    :param end_seek: 读取ima文件的结束位置
    :param start_lines: 当前读取ima文件的行数
    :param timestamp: 开启白名单的时间戳
    :param total_lines: 开启白名单的时间戳
    :return: 返回状态码 信息描述 最新的seek值
    """
    init_seek = start_seek
    curr_lines = start_lines
    conn_status = True
    try:
        read_ima(action, start_seek, init_seek, curr_lines, end_seek, start_lines, timestamp, total_lines)
    except Exception as e:
        conn_status = False
        if e.details() == "failed to connect to all addresses":
            if action == Action.PROTECT_AGENT and scheduler.get_job(READ_IMA):
                scheduler.remove_job(job_id=READ_IMA)
            LOGGER.error("Grpc failed to connect to oat_server. The oat_server may be dead !")
        else:
            LOGGER.error(e)
    finally:
        return conn_status


def get_host_info():
    """
    获取本机的identity和hostname
    :return:
    """
    host_name = socket.gethostname()
    is_reboot = check_reboot()
    host_info = {"host_ip": AGENT_IP, "host_name": host_name,
                 "port": str(AGENT_PORT), "host_sys": platform.system().upper(),
                 "restart": str(is_reboot), "identity": ""}
    # 私钥路径
    pub_key = IDENTITY_PATH + os.sep + "rsa.pub"
    with open(pub_key) as pk:
        key_data = pk.read()
        host_info["identity"] = key_data
    return host_info


def generate_identity():
    """
    生成公钥私钥
    :return:
    """
    try:
        # 私钥路径
        pri_key = IDENTITY_PATH + os.sep + "rsa.key"
        # 公钥路径
        pub_key = IDENTITY_PATH + os.sep + "rsa.pub"

        pri_key_path = pathlib.Path(pri_key)
        pub_key_path = pathlib.Path(pub_key)
        # 判断公钥和私钥这两个文件是否存在 如果存在直接返回 不需要再生成新的
        if pri_key_path.is_file() and pub_key_path.is_file():
            return

        # 获取一个伪随机数生成器
        random_generator = Random.new().read
        # 获取一个rsa算法对应的密钥对生成器实例
        rsa = RSA.generate(2048, random_generator)

        # 生成私钥并保存
        private_pem = rsa.exportKey()
        with open(pri_key, 'wb') as f:
            f.write(private_pem)

        # 生成公钥并保存
        public_pem = rsa.publickey().exportKey()
        with open(pub_key, 'wb') as f:
            f.write(public_pem)
    except Exception as e:
        LOGGER.error(e)


def encrypt_msg(msg):
    """
    加密数据
    :param msg:
    :return:
    """
    signature = ""
    try:
        # 私钥路径
        pri_key = IDENTITY_PATH + os.sep + "rsa.key"
        with open(pri_key) as pk:
            key_data = pk.read()

        rsa_key = RSA.importKey(key_data)
        signer = Signature_pkcs1_v1_5.new(rsa_key)
        sha256_obj = SHA256.new()
        sha256_obj.update(msg.encode('utf8'))
        sign = signer.sign(sha256_obj)
        signature = base64.b64encode(sign)
        signature = signature.decode("utf-8")
    except Exception as e:
        LOGGER.error(e)

    return signature


def append_audit_info(log_path, interval):
    """
    向audit.log中添加pid信息 供UEBA使用
    :param log_path: audit.log文件路径
    :param interval: 写audit.log间隔时间
    :return:
    """

    while True:
        # 每10秒执行一次
        time.sleep(int(interval))

        # 默认字符串以 PID###CMD 开头
        audit_info = "PID###CMD" + "\n"

        try:
            for proc in psutil.process_iter():
                p_info = proc.as_dict(attrs=['pid', 'name'])
                p_pid = p_info['pid']
                p_name = p_info['name']

                if p_pid and p_name:
                    # name中不能有空格 将空格替换成+
                    p_name = p_name.replace(" ", "+")
                    # 拼接信息的格式： pid + ### + pid_name
                    append_info = str(p_pid) + "###" + p_name + "\n"
                    # 组装信息
                    audit_info = audit_info + append_info
            # 拼接最终插入audit的内容
            audit_info = audit_info + "---END:---"

            # 如果audit.log文件存在且可写 通过追加方式 添加到log后面
            if os.access(log_path, os.F_OK) and os.access(log_path, os.W_OK):
                # 修改写入方式 防止触发ima度量
                cmd = "echo '" + audit_info + "' >> " + log_path
                os.popen(cmd)
            else:
                LOGGER.warning("The file " + audit_info + "  doesn't exist or doesn't have permission to write.")
        except Exception as e:
            LOGGER.error(e)


def test_port(ip, port):
    """
        检测OAT端口是都已经打开
    :param ip:
    :param port:
    :return:
    """
    is_connect = False
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ip, int(port)))
        s.shutdown(2)
        is_connect = True
        return is_connect
    except Exception as e:
        LOGGER.error(e)

        return is_connect


def agent_reg_to_oat():
    is_registered = False
    try:
        LOGGER.info(f"agent_reg_to_oat----->{OAT_IP}:{OAT_PORT}")
        with grpc.insecure_channel(f"{OAT_IP}:{OAT_PORT}") as channel:
            stub = oat_pb2_grpc.GreeterStub(channel)
            # agent去oat注册
            host_info = get_host_info()
            LOGGER.info(f"host_info---->{host_info}")
            resp = stub.AddClientHost(oat_pb2.AddClientHostRequest(client_host=host_info))
            LOGGER.info(f"AddClientHostResponse-------->{resp}")
            if resp.status == success.code:
                is_registered = True
                LOGGER.info("Registered was successful!")
            else:
                LOGGER.info("Failed register to oat_server!")
    except Exception as e:
        LOGGER.error(traceback.print_exc())
        LOGGER.info("Failed register to oat_server!")
    finally:
        return is_registered


def check_reboot():
    """
    检查本机是否重启过 如果重启过 向server发送更新重启指令 同时更新本地
    :return:
    """
    is_reboot = not_restarted.code
    try:
        # 查看库里面存的启动时间
        conn = conn_sqlite()
        real_time = int(psutil.boot_time())
        with conn:
            db_time = conn.execute("SELECT id,reboot_time FROM host_info LIMIT 1 ").fetchone()
            if db_time:  # 如果数据库存了主机启动时间 就进行比较
                if db_time[1] != real_time:
                    is_reboot = restarted.code
                    conn.execute(f"update host_info set reboot_time={real_time} where id={db_time[0]}")
            else:
                data = f"{real_time},'agent所在的宿主机启动时间'"
                conn.execute("insert into host_info (reboot_time, describe) values (%s)" % data)
    except Exception as e:
        LOGGER.error(e)

    return is_reboot


def start_protect_agent(timestamp):
    """
    开启防护，获取数据库存的当前seek，读取ima放入MQ
    :return:
    """
    try:
        LOGGER.info("ProtectAgent Ing....")
        curr_seek, curr_lines = get_current_seek()
        sum_rows, end_seek = latest_seek_rows(MEASUREMENTS_PATH)
        LOGGER.info(f"curr_seek-->{curr_seek},end_seek----->{end_seek}")
        action = Action.PROTECT_AGENT
        if curr_seek < end_seek:
            ima_to_server(action, curr_seek, end_seek, curr_lines, timestamp)
    except Exception as e:
        LOGGER.error(e)
