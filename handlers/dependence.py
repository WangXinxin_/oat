# -*- coding: utf-8 -*-
import os
import sys
import hashlib
import subprocess


current_dir = os.getcwd()
DEF_FILE = current_dir + os.sep + "dependence.txt"

if os.path.exists(DEF_FILE):
    # 删除该文件
    os.remove(DEF_FILE)


def dir_walk(path, file_handler):
    if os.path.isdir(path):
        for root, dirs, files in os.walk(path):
            # 遍历文件
            for name in files:
                file_name = os.path.join(root, name)
                file_dependence(file_name, file_handler)

            # 遍历目录
            for name in dirs:
                dir_name = os.path.join(root, name)
                # 遍历每一个目录
                dir_walk(dir_name, file_handler)
    else:
        file_dependence(path, file_handler)


def file_dependence(file_name, file_handler):
    # cmd = "objdump -x %s | grep NEEDED" % file_name
    # a = os.popen(cmd).read()
    try:
        cmd = "ldd " + file_name
        a = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        dependence = a.stdout.readlines()
        if len(dependence) == 1:
            pass
            # str_out = dependence[0].decode().strip()
            # print("[FILE]" + file_name + "," + str_out)
        else:
            # print("\n")
            # print("[FILE]" + file_name)
            file_handler.write("[FILE]" + file_name + "\n")
            for dep in dependence:
                dep = dep.decode().strip()
                if "=>" in dep:
                    bb = dep.split("=>")
                    so_name = bb[0].strip()
                    so_path = bb[1].strip()
                    if so_path == "not found":
                        file_handler.write("[NEED]" + so_name + "    [HASH] not found this file." + "\n")
                        # print("[NEED]" + so_name + "    [HASH] not found this file.")
                    else:
                        s_index = so_path.index(so_name)
                        so_path = so_path[:s_index] + so_name
                        so_hash = match(so_path)
                        file_handler.write("[NEEDED]" + so_path + "    [HASH]" + so_hash + "\n")
                        # print("[NEEDED]" + so_path + "    [HASH]" + so_hash)
                else:
                    s_index = dep.index("(")
                    so_name = dep[:s_index].strip()
                    file_handler.write("[NEEDED]" + so_name + "    [HASH] not found this file." + "\n")
                    # print("[NEEDED]" + so_name + "    [HASH] not found this file.")
            # print('\n')
            file_handler.write("\n")
    except Exception as e:
        print(e)


def CalcMD5(filepath):
    with open(filepath, 'rb') as ff:
        md5obj = hashlib.md5()
        md5obj.update(ff.read())
        hash = md5obj.hexdigest()
        return hash


def match(file_path, bytes_len=2048):
    md5_1 = hashlib.md5()  # 创建一个md5算法对象
    with open(file_path, 'rb') as f:  # 打开一个文件，必须是'rb'模式打开
        while 1:
            data = f.read(bytes_len)  # 由于是一个文件，每次只读取固定字节
            if data:                # 当读取内容不为空时对读取内容进行update
                md5_1.update(data)
            else:   # 当整个文件读完之后停止update
                break

    ret = md5_1.hexdigest()  # 获取这个文件的MD5值
    return ret


if __name__ == "__main__":
    len_argvs = len(sys.argv)
    if len_argvs < 2:
        print("Missing Parameters.")
        exit(0)
    else:
        file_dir = sys.argv[1]
        with open(DEF_FILE, mode="w", encoding="utf-8") as f:
            f.write("Start analyzing dependencies...\n\n")
            dir_walk(file_dir, f)
            f.write("End...")
