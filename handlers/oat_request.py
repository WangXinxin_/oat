# 本文件用来处理接口
import json
import threading
import time

from config import scheduler, MEASUREMENTS_PATH
from handlers.processing import check_reboot, ima_to_server, start_protect_agent
from utils.agent_log import LOGGER
from utils.constants import READ_IMA, success, failed, restarted, not_restarted, no_change, living, Action
from utils.whitelist_utils import latest_seek_rows
from common.rpc import agent_pb2, agent_pb2_grpc


def read_ima(action, rev_seek, end_seek, rev_curr_lines, sum_rows, timestamp):
    time.sleep(10)
    if rev_seek == end_seek:
        return agent_pb2.UpdateAgentWhitelistResponse(
            status=no_change.code, data="", msg=no_change.msg)
    if rev_seek < end_seek:
        send_status = ima_to_server(action, rev_seek, end_seek, rev_curr_lines, timestamp, sum_rows)
        if send_status:
            LOGGER.info("Update whitelist successfully ! ")
        else:
            LOGGER.info("Failed to update white list ! ")


class AgentGreeter(agent_pb2_grpc.AgentGreeter):
    def __init__(self):
        self.timestamp = int(time.time())

    def StartAgentProtection(self, request: agent_pb2.StartAgentProtectionRequest, context):
        """
        开启证实，向MQ发送ima文件
        :type request: object
        :param request:
        :param context:
        :return:
        目前开启证实时读的ima文件的new_seek值来自agent端缓存和oat传过来的new_seek 先以本地为主
        当接收到oat请求时seek值以oat为主没有的话从缓存里取
        """
        try:
            LOGGER.info(f"StartAgentProtection.........")
            rev_seek = request.new_seek
            curr_lines = request.new_lines
            LOGGER.info(f"rev_seek--->{rev_seek},rev_lines--->{curr_lines}")
            sum_rows, end_seek = latest_seek_rows(MEASUREMENTS_PATH)
            action = Action.PROTECT_AGENT
            if rev_seek <= end_seek:
                # 获取ima放入MQ里
                conn_status = ima_to_server(action, rev_seek, end_seek, curr_lines, self.timestamp)
                if conn_status:
                    # 开启定时读取ima文件
                    scheduler.add_job(start_protect_agent, "interval", seconds=10, id=READ_IMA, args=(self.timestamp,))
                    return agent_pb2.StartAgentProtectionResponse(status=success.code, msg=success.msg)
                else:
                    return agent_pb2.StartAgentProtectionResponse(status=failed.code, msg=failed.msg)
            if rev_seek > end_seek:
                return agent_pb2.StartAgentProtectionResponse(status=failed.code, msg=failed.msg)
        except Exception as e:
            LOGGER.error(e)
            return agent_pb2.StartAgentProtectionResponse(status=failed.code, msg=failed.msg)

    def StopAgentProtection(self, request: agent_pb2.StopAgentProtectionRequest, context):
        """
         停止证实
        1.停止读取ima文件
        """
        LOGGER.info("StopAgentProtection.......")
        try:
            if scheduler.get_job(READ_IMA):
                scheduler.remove_job(job_id=READ_IMA)
            return agent_pb2.StopAgentProtectionResponse(status=success.code, msg=success.msg)
        except Exception as e:
            LOGGER.error(e)
            return agent_pb2.StopAgentProtectionResponse(status=failed.code, msg=failed.msg)

    def UpdateAgentWhitelist(self, request: agent_pb2.UpdateAgentWhitelistRequest, context):
        """
        更新白名单接口，用来传输一次 ima片段
        :param request:
        :param context:
        :return:
        """
        try:
            LOGGER.info("UpdateAgentWhitelist.........")
            rev_seek = request.new_seek
            rev_curr_lines = request.new_lines
            LOGGER.info(f"rev_seek--->{rev_seek}--rev_curr_lines-->{rev_curr_lines}")
            sum_rows, end_seek = latest_seek_rows(MEASUREMENTS_PATH)
            LOGGER.info(f"end_seek--->{end_seek}--end_lines---->{sum_rows}")
            self.timestamp = int(time.time())
            data = {"timestamp": self.timestamp}
            action = Action.UPDATE_WHITELIST
            if rev_seek == end_seek:
                return agent_pb2.UpdateAgentWhitelistResponse(
                    status=no_change.code, data=json.dumps(data), msg=no_change.msg)
            if rev_seek < end_seek:
                t = threading.Thread(target=read_ima, args=(
                    action, rev_seek, end_seek, rev_curr_lines, sum_rows, self.timestamp))
                t.start()
                return agent_pb2.UpdateAgentWhitelistResponse(
                    status=success.code, data=json.dumps(data), msg=success.msg)
            return agent_pb2.UpdateAgentWhitelistResponse(
                status=failed.code, data=json.dumps(data), msg=failed.msg)
        except Exception as e:
            LOGGER.error(e)
            return agent_pb2.UpdateAgentWhitelistResponse(
                status=failed.code, msg=failed.msg, data=json.dumps({"timestamp": self.timestamp}))

    def CheckAgentAlive(self, request: agent_pb2.CheckAgentAliveRequest, context):
        """
        本接口用来 开启防护时oat检测agent是否在线
        :param request:
        :param context:
        :return:
        """
        LOGGER.info("CheckAgentAlive......")
        return agent_pb2.CheckAgentAliveResponse(status=living.code, msg=living.msg)

    def HostAgentRestart(self, request: agent_pb2.HostAgentRestartRequest, context):
        """
        检测主机是否重启过
        :param request:
        :param context:
        :return: 0：没有重启过 1：重启过
        """
        LOGGER.info(f"CheckHostAgentRestart........")
        is_reboot = check_reboot()
        if is_reboot:  # 发送重置指令
            restart_status = restarted.code
            msg = restarted.msg
        else:
            restart_status = not_restarted.code
            msg = not_restarted.msg
        return agent_pb2.HostAgentRestartResponse(
            status=success.code, msg=msg, restart=restart_status)
