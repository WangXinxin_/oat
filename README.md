运行部署阅读 一、二、三、四项即可，开发阅读全部
# 一、安装agent依赖
pip3 install -r requirements.txt
# 二、配置文件

将项目根目录下的tagent.ini，logging-linux.yaml或logging-windows.yaml文件复制到/8lab/config文件夹下

- 注：
  - 如果主机是linux系统则复制logging-linux.yaml文件即可 
  - 如果主机是windows系统则复制logging-windows.yaml文件即可

# 三、启动

python3 main.py

# 四、查看agent当前版本
python3 main.py -v

# 五、rpc/proto/oat.proto 生成

```python
cd common/rpc/proto
python3 -m grpc_tools.protoc --python_out=.. --grpc_python_out=.. -I. agent.proto
python3 -m grpc_tools.protoc --python_out=.. --grpc_python_out=.. -I. oat.proto
```

# 六、日志模块

## 日志文件路径

### linux 系统
```
/8lab/log/oat/tagent.log
```
/8lab/log/oat/ 目录只存放 oat_agent 项目的日志

### windows 系统
```
C:\8lab\log\oat\tagent.log
```

## 日志配置文件
```
oat_agent/logging-linux.yaml  (linux 系统)
oat_agent/logging-windows.yaml  (windows 系统)
```

### linux 系统实际路径
```
/8lab/conf/
```

### windows 系统实际路径
```
C:\8lab\conf
```

oat_agent/tagent.ini 文件新增了如下配置
```
[log]
; linux 日志文件路径
log_config_file_linux = /8lab/conf/logging-linux.yaml
; windows 日志文件路径
log_config_file_windows = C:\8lab\conf\logging-windows.yaml
```
实际运行时确保  /8lab/conf/tagent.ini 中有此配置项
如果是 linux 系统，保证有 log_config_file_linux 配置项
如果是 windows 系统，保证有 log_config_file_windows 配置项


## 动态修改日志级别不重启服务

oat_agent 程序启动后有一个线程监控日志配置文件 /8lab/conf/logging-linux.yaml (linux系统)  
 C:\8lab\conf\logging-windows.yaml  (windows 系统)

如果配置文件有改动，程序自动重新加载日志模块，改动的配置生效

修改  /8lab/conf/logging-linux.yaml  控制台和文件日志级别
控制台日志配置
```
handlers:
    consoleHandler:
        class: logging.StreamHandler
        level: DEBUG
        stream: 'ext://sys.stdout'
        formatter: defaultFormatter
```
文件日志配置
```
    fileHandler:
        class: logging.handlers.EnhancedRotatingFileHandler
        level: DEBUG
        formatter: defaultFormatter
        filename: '/8lab/log/oat/tagent.log'
        when: 'MIDNIGHT'
        interval: 1
        backupCount: 100000
        maxBytes: 209715200 #200M
```
只修改 level 的值

## 日志级别说明

| 日志等级（level） | 描述                                                         |
| ----------------- | ------------------------------------------------------------ |
| DEBUG             | 最详细的日志信息，典型应用场景是 问题诊断                    |
| INFO              | 信息详细程度仅次于DEBUG，通常只记录关键节点信息，用于确认一切都是按照我们预期的那样进行工作 |
| WARNING           | 当某些不期望的事情发生时记录的信息（如，磁盘可用空间较低），但是此时应用程序还是正常运行的 |
| ERROR             | 由于一个更严重的问题导致某些功能不能正常运行时记录的信息     |
| CRITICAL          | 当发生严重错误，导致应用程序不能继续运行时记录的信息         |


## 日志方案
- 保留7天日志  
  每天 00:01:05 的时候将过期的日志文件删除

- 每个日志文件不超过 200M


# 七、相关接口
##  oat_server调用oat_agent

- 更新白名单
- 可开启防护
- 关闭防护
- 检测agent是否在线
- 检测主机是否重启过

## oat_agent调用oat_server

- 注册agent

