# -*- coding: utf-8 -*-
import threading
import time
import psutil
from utils.agent_log import LOGGER


class CpuUsage(object):
    _instance_lock = threading.Lock()
    _cpu_usage = 0.0

    def __init__(self, pid):
        self.process = psutil.Process(pid)

    def __new__(cls, *args, **kwargs):
        if not hasattr(CpuUsage, "_instance"):
            with CpuUsage._instance_lock:
                if not hasattr(CpuUsage, "_instance"):
                    CpuUsage._instance = object.__new__(cls)
        return CpuUsage._instance

    @staticmethod
    def get_cpu_usage():
        return CpuUsage._cpu_usage

    def calc_cpu_usage(self):
        while True:
            try:
                CpuUsage._cpu_usage = self.process.cpu_percent(2)
            except Exception as e:
                LOGGER.error(e)
                time.sleep(2)


if __name__ == "__main__":
    obj = CpuUsage(2257)
    threading.Thread(target=obj.calc_cpu_usage).start()
    while True:
        print(CpuUsage.get_cpu_usage())
        time.sleep(2)
