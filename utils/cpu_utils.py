# -*- coding: utf-8 -*-
import platform
import subprocess
import psutil
from utils.agent_log import LOGGER


CPU_USAGE = 0.0


def get_cpu_percent3(pid):
    p = psutil.Process(pid)
    current_cpu = p.cpu_percent(2)
    return current_cpu


def get_cpu_percent2(process_id):
    cpu_usage = 0.0
    try:
        sys_name = platform.system().upper()
        if sys_name == 'LINUX':  # 目前只支持linux
            cmd = '''ps aux |grep %s | awk 'BEGIN{ FS=" ";OFS="," }{print $2, $3, $4}' ''' % str(process_id)
            cmd_outs = subprocess.getoutput(cmd)
            cmd_outs = cmd_outs.split("\n")
            for cmd_out in cmd_outs:
                if cmd_out:
                    ps_data = cmd_out.split(",")
                    ps_pid = ps_data[0]
                    if int(ps_pid) == process_id:
                        cpu_usage = float(ps_data[1])
                        # LOGGER.info("pid {} usage:{}".format(str(ps_pid), str(cpu_usage)))
                        break
    except Exception as e:
        cpu_usage = 99.99
        LOGGER.error(e)

    return cpu_usage


def get_cpu_percent(pid):
    cpu_usage = 0.0
    try:
        cmd_list = ['top', '-bn', '2', '-p', str(pid), '-d', '0.3']
        top_info = subprocess.Popen(cmd_list, stdout=subprocess.PIPE)
        out, err = top_info.communicate()
        if not err:
            # output info get from console has many unicode escape character
            # such as \x1b(B\x1b[m\x1b[39;49m\x1b[K\n\x1b(B\x1b[m
            # use decode('unicode-escape') to process
            out_info = out.decode('unicode-escape').strip()
            lines = out_info.split('\n')
            if len(lines) == 17:    # top前5行时机器状态+空行+行标+pid具体信息 8行 两次中间有间隔 8+1+8 = 17
                detail = lines[16]
                detail = detail.strip().split()
                if pid == int(detail[0]):
                    cpu_usage = float(detail[8])
                    LOGGER.info(detail)
            else:
                LOGGER.info("pid:" + str(pid) + " not exist.")
        else:
            LOGGER.error("top -n 1 -p " + str(pid) + " failed.")
            cpu_usage = 99.99
    except Exception as e:
        cpu_usage = 99.99
        LOGGER.error(e)

    return cpu_usage


def calc_cpu_usage(pid):
    global CPU_USAGE
    while True:
        try:
            cmd_list = ['top', '-bn', '2', '-p', str(pid), '-d', '0.1']
            top_info = subprocess.Popen(cmd_list, stdout=subprocess.PIPE)
            out, err = top_info.communicate()
            if not err:
                # output info get from console has many unicode escape character
                # such as \x1b(B\x1b[m\x1b[39;49m\x1b[K\n\x1b(B\x1b[m
                # use decode('unicode-escape') to process
                out_info = out.decode('unicode-escape').strip()
                lines = out_info.split('\n')
                if len(lines) == 17:  # top前5行时机器状态+空行+行标+pid具体信息 8行 两次中间有间隔 8+1+8 = 17
                    detail = lines[16]
                    detail = detail.strip().split()
                    if pid == int(detail[0]):
                        CPU_USAGE = float(detail[8])
                else:
                    LOGGER.info("pid:" + str(pid) + " not exist.")
            else:
                LOGGER.error("top -n 1 -p " + str(pid) + " failed.")
        except Exception as e:
            CPU_USAGE = 99.99
            LOGGER.error(e)


if __name__ == "__main__":
    b = psutil.Process(2257)
    while True:
        a = b.cpu_percent(2)
        print(a)
