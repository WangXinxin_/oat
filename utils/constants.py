

class Status(object):
    def __init__(self, code, msg):
        self.code = code
        self.msg = msg


# 一般
success = Status(200, 'success')
failed = Status(400, 'failed')
restarted = Status(1, "重启过")
not_restarted = Status(0, "未重启过")
no_change = Status(4004, "未发生变化")
living = Status(1, "living...")
dead = Status(0, "the host is dead.")


class Action:
    PROTECT_AGENT = "ProtectAgent"
    UPDATE_WHITELIST = "UpdateWhitelist"


READ_IMA = "read_ima"  # 本变量是读取ima文件任务的job_id
IMA_EXCHANGE = "AGENT_IMA"
IMA_EXCHANGE_TYPE = "direct"
ALARM_QUEUE = "ima_alarm"
ALARM_ROUTING = "ima_alarm_routing"
WHITELIST_QUEUE = "ima_whitelist"
WHITELIST_ROUTING = "ima_whitelist_routing"
HEARTBEAT_QUEUE = "heartbeat"
HEARTBEAT_ROUTING = "heartbeat_routing"
USE_MQ = 1
USE_GRPC = 0
