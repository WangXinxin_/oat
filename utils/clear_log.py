# -*- coding: utf-8 -*-
import os
import platform
import time
from copy import deepcopy

from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.interval import IntervalTrigger

"""
删除日志，日志文件夹下**.log 总的大小不超过300M = 1024 * 1024 * 300
删除逻辑， 获取文件大小，创建时间，按照创建时间排序，
日志文件累加 大于300M， 删除最早创建的日志
"""


def log_file_dir():
    if platform.system() == 'Linux':
        LOG_DIR = os.path.sep + "8lab" + os.path.sep + "log"
    elif platform.system() == 'Windows':
        LOG_DIR = "c:" + os.path.sep + "8lab" + os.path.sep + "log"
    else:  # for MAC os
        LOG_DIR = os.path.sep + "8lab" + os.path.sep + "log"
    return LOG_DIR


def log_path(file_dir):
    try:
        from config import LOG_DIR_SIZE
        file_dir_size = LOG_DIR_SIZE
    except:
        file_dir_size = 1024 * 1024 * 300

    if os.path.exists(file_dir):
        file_li = []
        for dirpath, dirnames, filenames in os.walk(file_dir):
            for filename in filenames:
                file = os.path.join(file_dir, filename)
                if file.endswith('.zip'):
                    file_li.append({
                            'file_name': file,  # 文件路径名
                            'create_time': os.stat(file).st_ctime,  # 创建时间
                            'file_size': os.stat(file).st_size  # 字节
                        })

        file_li = sorted(file_li, key=lambda i: i['create_time'], reverse=True)
        if not file_li:  # 日志中没有文件
            return

        idx = '0'
        current_size = 0
        for idx, dic in enumerate(deepcopy(file_li)):  # 获取文件列表累加大于300M后的下标
            current_size += dic['file_size']
            print(f'current_size：{current_size}  file_dir_size:{file_dir_size}')
            if current_size >= file_dir_size:
                idx = idx
            else:
                idx = '0'

        if isinstance(idx, int):  # 获取文件列表大于300M之前的文件
            for i in file_li[idx:]:
                delete_flie(i['file_name'])


def delete_flie(file_path):
    try:
        print(file_path)
        os.remove(file_path)
    except Exception as e:
        print(f"permission denied-{e}")


def sched_clear_log():
    scheduler = BlockingScheduler()
    log_dir = log_file_dir()
    trigger = IntervalTrigger(days=1)
    scheduler.add_job(log_path, trigger, args=[log_dir])
    scheduler.start()

    try:
        while True:
            time.sleep(5)
    except (KeyboardInterrupt, SystemExit):
        scheduler.shutdown()


if __name__ == '__main__':
    sched_clear_log()
