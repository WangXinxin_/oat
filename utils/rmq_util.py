"""'
rabbitmq_utils
"""
import pika
from config import RABBITMQ_IP, RABBITMQ_PORT, RABBITMQ_USER, RABBITMQ_PASS
from utils.agent_log import LOGGER
from utils.constants import success, failed, IMA_EXCHANGE_TYPE


def conn_rabbitmq():
    for i in range(5):  # 如果连接失败则重试5次
        try:
            user_pass = pika.PlainCredentials(RABBITMQ_USER, RABBITMQ_PASS)
            connection = pika.BlockingConnection(pika.ConnectionParameters(
                host=RABBITMQ_IP, port=RABBITMQ_PORT, virtual_host='/', credentials=user_pass))

            return connection
        except Exception as e:
            LOGGER.error(e)


def pub_msg(msg, exchange, queue, routing_key, exchange_type=IMA_EXCHANGE_TYPE, rev_properties=None):
    """
    向rabbitMQ发送ima片段
    发布信息是绑定交换机，一个agent一个队列，队列之间互不影响，方便agent初始化
    :param exchange_type: 默认direct模式
    :param msg: 要放在队列里的json类型的消息
    :param exchange:
    :param queue:
    :param routing_key:
    :param rev_properties:
    :return:
    """
    try:
        connection = conn_rabbitmq()
        if rev_properties:
            properties = rev_properties
        else:
            properties = pika.BasicProperties(delivery_mode=2)
        if connection:  # 如果连接成功就将数据发布到队列中
            channel = connection.channel()
            # durable = True 代表exchange持久化存储，False 非持久化存
            channel.exchange_declare(exchange=exchange, exchange_type=exchange_type, durable=True)
            channel.queue_declare(queue=queue, durable=True)
            channel.queue_bind(exchange=exchange, queue=queue, routing_key=routing_key)
            channel.confirm_delivery()
            channel.basic_publish(
                exchange=exchange, routing_key=routing_key,
                body=msg, properties=properties)
            connection.close()
            return success.code, success.msg
        else:
            LOGGER.error("Connection queue failed. The queue may be dead !")
            return failed.code, failed.msg
    except Exception as e:
        LOGGER.error(e)
        return failed.code, failed.msg
