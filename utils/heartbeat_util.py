# -*- coding: utf-8 -*-
import json
import time
import grpc
import pika

from retrying import retry

from common.rpc import oat_pb2, oat_pb2_grpc
from .agent_log import LOGGER
from config import HEARTBEAT, AGENT_IP, TRANSFER_MODE, OAT_IP, OAT_PORT
from .constants import IMA_EXCHANGE, HEARTBEAT_QUEUE, HEARTBEAT_ROUTING, USE_MQ, success


def heartbeat_while():
    """
    心跳信号 定时向rabbitMQ发送心跳信息 表示服务正常
    IMA_EXCHANGE暂定和读ima一起
    :return:
    """
    LOGGER.info("heartbeat thread working...")

    while True:
        # 发送主机过期时间到队列{"host_ip":"192.168.3.63", "alive_time":int(time.time())}
        if TRANSFER_MODE == USE_MQ:  # 将心跳传到MQ
            send_heartbeat(AGENT_IP)
        else:
            alive_msg = send_heartbeat(AGENT_IP)
            yield alive_msg
        # 每隔配置的时间发送一次心跳
        time.sleep(HEARTBEAT)


@retry(wait_fixed=10000)  # 遇到error循环每隔10分钟重试3次查找存活的oat_server发送心跳
def rpc_send_heartbeat():
    for i in range(3):
        with grpc.insecure_channel(f"{OAT_IP}:{OAT_PORT}") as channel:
            stub = oat_pb2_grpc.GreeterStub(channel)
            # 将数据通过grpc发送给server
            resp, call = stub.HeartBeat.with_call(
                heartbeat_while(),
                compression=grpc.Compression.Gzip)
            LOGGER.info(resp.status)
        if resp.status == success.code:
            break
        time.sleep(3)


# 一旦发生给oat_server传送异常时，一共尝试三次每次间隔3秒
def heartbeat():
    """
    心跳信号 定时向rabbitMQ发送心跳信息 表示服务正常
    IMA_EXCHANGE暂定和读ima一起
    :return:
    """
    try:
        LOGGER.info("heartbeat thread working...")
        if TRANSFER_MODE == USE_MQ:  # 将心跳传到MQ
            heartbeat_while()
        else:
            rpc_send_heartbeat()
    except Exception as e:
        if e.details() == "Socket closed" or e.details() == "failed to connect to all addresses":
            LOGGER.error("Agent failed to connect to oat_server ! ")
            LOGGER.error("Trying to connect to oat_server ! ")
        else:
            LOGGER.info(e)


def send_heartbeat(host_ip):
    """
    向MQ发送当前心跳时间
    :param host_ip: 本机ip
    :return:
    """
    try:
        LOGGER.info("send_heartbeat_to_mq......")
        alive_msg = {"host_ip": host_ip, "alive_time": int(time.time())}
        properties = pika.BasicProperties(delivery_mode=1, expiration="25000")
        if TRANSFER_MODE == USE_MQ:
            from .rmq_util import pub_msg
            pub_msg(
                msg=json.dumps(alive_msg), exchange=IMA_EXCHANGE, queue=HEARTBEAT_QUEUE,
                routing_key=HEARTBEAT_ROUTING, rev_properties=properties)
        else:  # 通过grpc传递给oat_server
            heartbeat_request = oat_pb2.HeartBeatTreamRequest(heartbeat=json.dumps(alive_msg))
            return heartbeat_request

    except Exception as e:
        LOGGER.error(e)
