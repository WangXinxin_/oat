from config import MEASUREMENTS_PATH
from sqlite_dbs import conn_sqlite
from utils.agent_log import LOGGER


def get_current_seek():
    """
    查询ima文件当前读到的seek值和行号
    """
    try:
        conn = conn_sqlite()
        with conn:
            next_seek = conn.execute(
                "select current_seek,curr_line_num from agent_ima_record where file_name=(?)",
                (MEASUREMENTS_PATH,)).fetchone()
            curr_seek = next_seek[0]
            curr_line_num = next_seek[1]
        return curr_seek, curr_line_num
    except Exception as e:
        LOGGER.error(e)


def update_seek(file_path, ima_seek, curr_lines):
    """
    根据文件名更新当前读到的seek值
    :param file_path: 文件路径
    :param ima_seek: 最新的seek值
    :param curr_lines: seek值对应的行数
    :return:
    """
    try:
        conn = conn_sqlite()
        with conn:
            # 将最新seek值放到数据库里 该记录存在即更新
            conn.execute("replace into agent_ima_record(file_name,current_seek,curr_line_num) VALUES (?,?,?)",
                         (file_path, ima_seek, curr_lines))
    except Exception as e:
        LOGGER.error(e)
