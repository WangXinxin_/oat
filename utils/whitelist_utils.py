from utils.agent_log import LOGGER
import subprocess


def latest_seek_rows(file_path):
    try:
        latest_info = subprocess.getoutput("wc -lc %s" % file_path).split(" ")
        latest_rows = latest_info[0]
        latest_seek = latest_info[1]
        return int(latest_rows), int(latest_seek)
    except Exception as e:
        LOGGER.error(e)
        return 0, 0

