# -*- coding: utf-8 -*-
"""
  logger
  使用说明：此日志是对logging的封装

  写日志到Console和带时间切割策略的File:
      logger = make_8lab_console_time_logger()

"""

import os
import sys
import platform
import logging
import logging.handlers
import logging.config
import time
import yaml
import subprocess
import threading

from datetime import date, datetime, timedelta
from configparser import ConfigParser
from utils.log_base import EnhancedRotatingFileHandler

DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
LOG_FORMAT = '%(asctime)s [%(levelname)s] [%(module)s:%(funcName)s] [%(lineno)d] - %(message)s'
LOG_LEVEL = logging.DEBUG
FORMATTER = logging.Formatter(LOG_FORMAT, DATE_FORMAT)
sys_name = platform.system().upper()

if sys_name == 'LINUX':
    LOG_DIR = os.path.sep + "8lab" + os.path.sep + "log" + os.path.sep + "oat"
    LOG_FILENAME = os.path.join(LOG_DIR, "tagent.log")
    config_path = "/8lab/conf/oat_agent/"
elif sys_name == "WINDOWS":
    sys_drive = os.getenv("SystemDrive")
    LOG_DIR = sys_drive + os.path.sep + "8lab" + os.path.sep + "log" + os.path.sep + "oat"
    LOG_FILENAME = os.path.join(LOG_DIR, "tagent.log")
    config_path = sys_drive + os.sep + "8lab" + os.sep + "conf" + os.sep + "oat_agent"

else:  # for MAC os
    LOG_DIR = os.path.sep + "8lab" + os.path.sep + "log"
    LOG_FILENAME = os.path.sep + "8lab" + os.path.sep + "log" + os.path.sep + "tagent.log"
    config_path = "/8lab/conf/oat_agent/"

if not os.path.exists(LOG_DIR):
    os.makedirs(LOG_DIR)

if not os.path.exists(config_path):
    os.makedirs(config_path)

# 1.创建配置解析器对象
config = ConfigParser()
# 2.读取配置文件名
conf_path = config_path + os.sep + "tagent.ini"

if sys_name == "WINDOWS":
    config.read(conf_path, encoding='utf-8-sig')       # windows文本文件有bom标识 所以encode用utf-8-sig
    YAML_LOG_CONFFILE = config.get('log', 'log_config_file_windows')
elif sys_name == "LINUX":
    config.read(conf_path, encoding='utf-8')
    YAML_LOG_CONFFILE = config.get('log', 'log_config_file_linux')

def get_log_config_file(file_name):
    """ 获取日志文件内容"""
    with open(file_name, 'r') as f:
        # 读取日志配置文件
        yarm_config_data = f.read()
        dict_config_data = yaml.load(yarm_config_data)

        # 重新加载日志模块
        logging.config.DictConfigurator(dict_config_data).configure()
        LOGGER.info(f"finished configured the logging")


def log_config_monitor():
    """ 日志配置监控，如果配置变更，动态加载日志模块"""
    while True:
        time.sleep(5)        
        LOGGER.info(f"start log config monitor")
        get_log_config_file(YAML_LOG_CONFFILE)


def make_8lab_console_time_logger():
    """
    定义日志对象,实现同时写入Console和文件"/8lab/log/tagent.log",并且可以切割
    :return:  logger
    """

    logging.handlers.EnhancedRotatingFileHandler = EnhancedRotatingFileHandler

    with open(YAML_LOG_CONFFILE, 'r') as stream:
        config = yaml.load(stream)

    logging.config.dictConfig(config)

    common_logger = logging.getLogger("tagent")        

    return common_logger


def task_delete_log_flie():
    """
    删除过期日志文件
    """
    expiration_time = datetime.now() - timedelta(days=7) #  日志保留7天
    expiration_time_str = expiration_time.strftime("%Y-%m-%d")
    LOGGER.debug(f"expiration_time_str:  {expiration_time_str}")
    delete_file_cmd = f"find . ! -newermt \"{expiration_time_str}\" -not -path  ./tagent.log -delete"
    LOGGER.info(f"delete_file_cmd: {delete_file_cmd}")
    cmd_result = subprocess.run(delete_file_cmd, universal_newlines=True,
                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, cwd=LOG_DIR)
    if cmd_result.stderr:
        err = f"delete log file failed {cmd_result.stderr}"
        LOGGER.error(err)

    else:
        LOGGER.info(f"success delete before {expiration_time_str} log file")

    delete_trustfile_log_cmd = f"find ./ -type f -name 'trustfile.*' ! -newermt  '%s' -delete" %(expiration_time_str,)
    LOGGER.info(f"delete_trustfile_log_cmd: {delete_trustfile_log_cmd}")
    delete_trustfile_result = subprocess.run(delete_trustfile_log_cmd, universal_newlines=True,
                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, cwd='/8lab/log')
    if delete_trustfile_result.stderr:
        err = f"delete trustfile log file failed {cmd_result.stderr}"
        LOGGER.error(err)

    else:
        LOGGER.info(f"success delete before {expiration_time_str} trustfile log file")


if not os.path.isdir(LOG_DIR):
    os.makedirs(LOG_DIR)

if not os.path.isfile(LOG_FILENAME):
    open(LOG_FILENAME, 'w+')

LOGGER = make_8lab_console_time_logger()
