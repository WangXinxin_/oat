# -*- coding: utf-8 -*-
import socket
import fcntl
import struct
import platform
from utils.agent_log import LOGGER


# 获取网卡名称和其ip地址，不包括回环
def get_ip_by_nic(if_name):
    """
    :param if_name:
    :return:
    """
    host_ip = None
    try:
        # 判断操作系统
        sys_name = platform.system().upper()
        if sys_name != "WINDOWS":
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            host_ip = socket.inet_ntoa(fcntl.ioctl(
                s.fileno(),
                0x8915,  # SIOCGIFADDR
                struct.pack(b'256s', if_name[:15].encode("utf-8"))
            )[20:24])
        else:
            LOGGER.error("get_ip_by_nic network not support windows.")
    except Exception as e:
        LOGGER.error("get ip by nic failed.")
        LOGGER.error(e)

    return host_ip


def get_host_ip():
    sock = None
    local_ip = None
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.connect(('8.8.8.8', 80))
        local_ip = sock.getsockname()[0]
    except Exception as e:
        LOGGER.error(e)
    finally:
        if sock:
            sock.close()

    return local_ip


if __name__ == '__main__':
    agent_ip = get_ip_by_nic("lo")
    LOGGER.info(f"get ip by nic----->{agent_ip}")
