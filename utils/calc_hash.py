# -*- coding: utf-8 -*-
import os
import sys
import hashlib


def calc_sha1(file_path):
    # 判断文件是否存在
    if file_path and os.path.isfile(file_path):
        with open(file_path, 'rb') as f:
            sha1obj = hashlib.sha1()
            sha1obj.update(f.read())
            hash_str = sha1obj.hexdigest()
            return hash_str
    else:
        return "0" * 40


def calc_md5(file_path):
    # 判断文件是否存在
    if file_path and os.path.isfile(file_path):
        with open(file_path, 'rb') as f:
            md5obj = hashlib.md5()
            md5obj.update(f.read())
            hash_str = md5obj.hexdigest()
            return hash_str
    else:
        return "0" * 40


if __name__ == "__main__":
    if len(sys.argv) == 2:
        hash_file = sys.argv[1]
        if not os.path.exists(hash_file):
            hash_file = os.path.join(os.path.dirname(__file__), hash_file)
            if not os.path.exists(hash_file):
                print("cannot found file")
            else:
                calc_md5(hash_file)
        else:
            calc_md5(hash_file)
    else:
        print("no filename")
