# -*- coding: utf-8 -*-
import argparse
import os
import grpc
import time
import platform
import threading
from concurrent import futures

from apscheduler.triggers.cron import CronTrigger
from config import AGENT_IP, OAT_IP, OAT_PORT, scheduler, AGENT_PORT, MEASUREMENTS_PATH
from handlers.oat_request import AgentGreeter
from common.rpc import agent_pb2_grpc
from sqlite_dbs import create_tables
from utils.agent_log import LOGGER, log_config_monitor, task_delete_log_flie
from utils.cpu_calc import CpuUsage

from handlers.processing import calc_local_pcr, generate_identity, append_audit_info, agent_reg_to_oat, test_port, \
    check_reboot
from utils.heartbeat_util import heartbeat
from utils.sql_utils import update_seek

if __name__ == '__main__':

    # 解析参数 暂只支持version
    parser = argparse.ArgumentParser()
    VERSION = "1.0.0"
    parser.add_argument('-v', '--version', action='version', version='Agent Server Version: ' + str(VERSION))
    args = parser.parse_args()
    create_tables()
    # 检查是否已有公钥对 如果不存在就生成新私钥对
    generate_identity()
    for i in range(5):
        # 检测oat服务是否启动
        is_connect = test_port(OAT_IP, OAT_PORT)
        if is_connect:
            LOGGER.info("Found oat server !")
            # 向oat注册本agent
            is_registered = agent_reg_to_oat()
            if is_registered:
                LOGGER.info("Registered to oat server successfully ! ")
                try:
                    LOGGER.info("Start up agent...")
                    is_reboot = check_reboot()
                    if is_reboot:  # 如果宿主机有重启则将seek值置为0
                        update_seek(MEASUREMENTS_PATH, 0, 0)
                    # 开启grpc
                    server = grpc.server(futures.ThreadPoolExecutor(max_workers=50))
                    agent_pb2_grpc.add_AgentGreeterServicer_to_server(AgentGreeter(), server)
                    server.add_insecure_port(f"{AGENT_IP}:{AGENT_PORT}")
                    server.start()
                    # 启动心跳服务
                    threading.Thread(target=heartbeat).start()
                    # 删除过期的日志文件
                    delete_log_file_trigger = CronTrigger(
                        year="*", month="*", day="*", hour="0", minute="1", second="5"
                    )
                    scheduler.add_job(
                        task_delete_log_flie,
                        trigger=delete_log_file_trigger,
                        args=[],
                        name="delete_overdue_log_file",
                    )
                    scheduler.start()
                    #  启动日志配置监控线程
                    threading.Thread(name='log_config_monitor', target=log_config_monitor).start()
                    # 获取当前进程pid
                    agent_pid = os.getpid()
                    LOGGER.info("trust agent pid:" + str(agent_pid))

                    # 计算cpu使用率
                    calc_cpu_obj = CpuUsage(agent_pid)
                    threading.Thread(target=calc_cpu_obj.calc_cpu_usage).start()

                    # 判断操作系统
                    sys_name = platform.system().upper()
                    if sys_name == 'LINUX':
                        # 开始计算本地pcr
                        threading.Thread(target=calc_local_pcr).start()
                    
                        from config import AUDIT_INTERVAL, AUDIT_PATH
                        # 如果audit间隔时间为0 说明不向audit.log中添加pid信息
                        if AUDIT_INTERVAL != 0:
                            # 修改audit.log
                            threading.Thread(target=append_audit_info, args=(AUDIT_PATH, AUDIT_INTERVAL)).start()
                    LOGGER.info("start receiving task...")
                except:
                    import traceback
                    traceback.print_exc()
                while 1:
                    time.sleep(1)
            else:
                LOGGER.error("Registered to oat_server failed, trust agent startup failed!!!")
        else:
            LOGGER.error("Can not find oat server, trust agent startup failed!!!")
        time.sleep(3)