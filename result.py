# -*- coding: utf-8 -*-


class Result:
    """
    统一请求返回结果
    """
    def __init__(self, code, msg, result):
        self.code = code    # 0表示正确 其余整数表示错误
        self.msg = msg  # 正确或错误信息
        self.result = result    # 具体结果内容

    def to_dict(self):
        return {
            "code": self.code,
            "msg": self.msg,
            "result": self.result
        }
