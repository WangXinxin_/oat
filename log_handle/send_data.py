# -*- coding: utf-8 -*-
import time
from kafka import KafkaProducer

from .file_scaner import LOG_QUEUE
from utils.agent_log import LOGGER
from config import BOOTSTRAP_SERVERS, TOPIC, MSG_KEY


def queue_handler():
    if BOOTSTRAP_SERVERS.endswith(","):
        hosts = BOOTSTRAP_SERVERS[:-1].split(",")
    else:
        hosts = BOOTSTRAP_SERVERS.split(",")
    producer = KafkaProducer(bootstrap_servers=hosts)
    send_num = 0
    while True:
        try:
            data = LOG_QUEUE.get()
            send_num = send_num + 1
            data = data.encode('utf-8')     # str转bytes
            # 发送数据到kafka
            producer.send(topic=TOPIC, key=MSG_KEY, value=data)
            if send_num == 20:  # 每发送20个数据 sleep 0.1秒
                send_num = 0
                time.sleep(0.1)
        except Exception as e:
            LOGGER.error(e)
