# -*- coding: utf-8 -*-
import time
import pickle
import os
import queue


# 保存解析以后的数据 发送至kafka中
LOG_QUEUE = queue.Queue(maxsize=1000)


class LogScanner:
    def __init__(self, log_file, seek_file='/tmp/log-inc-scan.seek.temp'):
        self.log_file = log_file
        self.seek_file = seek_file
        self.start_collecting = False   # 标记是否开始采集日志
        self.single_data = ""

    def scan(self):
        seek = self._get_seek()
        """
        file_mtime = os.path.getmtime(self.log_file)
        if file_mtime <= seek['time']:
            print('file mtime not change since last scan')
            seek['time'] = file_mtime
            self._dump_seek(seek)
            return []
        """

        file_size = os.path.getsize(self.log_file)
        if file_size <= seek['position']:
            print('file size not change since last scan')
            seek['position'] = file_size
            self._dump_seek(seek)
            return []

        print('file changed,start to scan')
        matchs = []
        with open(self.log_file, 'rb') as logfd:
            # 设置seek
            logfd.seek(seek['position'], os.SEEK_SET)
            # TODO 解析内容
            for line in logfd:
                # byte转string
                line = line.decode("utf-8")

                if line == "---END:---\n" and self.start_collecting is False:
                    # 标识开始采集
                    self.start_collecting = True
                    # 清空数据
                    self.single_data = ""
                elif line == "---END:---\n" and self.start_collecting is True:
                    # 标识结束采集
                    self.start_collecting = False
                    self.single_data = self.single_data + line
                    # print
                    print(self.single_data)
                    self.single_data = ""

                    # 更新seek
                    seek = {'time': time.time(), 'position': logfd.tell()}
                    self._dump_seek(seek)
                else:
                    if self.start_collecting:
                        self.single_data = self.single_data + line

            # 更新seek
            seek = {'time': time.time(), 'position': logfd.tell()}
            self._dump_seek(seek)
        return matchs

    def _get_seek(self):
        seek = {'time': time.time(), 'position': 0}
        if os.path.exists(self.seek_file):
            with open(self.seek_file, 'rb') as seekfd:
                try:
                    seek = pickle.load(seekfd)
                except Exception as e:
                    print(e)
        print(seek)
        return seek

    def _dump_seek(self, seek):
        with open(self.seek_file, 'wb') as seekfd:
            pickle.dump(seek, seekfd)

    def reset_seek(self):
        self._dump_seek({'time': time.time(), 'position': 0})


def log_scan(file_path):
    scanner = LogScanner(file_path)
    scanner.reset_seek()
    while True:
        matchs = scanner.scan()
        for match in matchs:
            print('fond at:' + match.group(1) + ' content:' + match.group(0))
        time.sleep(5)


if __name__ == "__main__":
    log_path = '/home/kaizi/work/bucket/gd_202009/py-tagent/audit.log'
    log_scan(log_path)
